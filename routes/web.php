<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/transport-logistics', [App\Http\Controllers\HomeController::class, 'transportLogistics'])->name('transport-logistics');

Route::get('/parcel-delivery', [App\Http\Controllers\HomeController::class, 'parcelDelivery'])->name('parcel-delivery');

Route::get('/european-distribution', [App\Http\Controllers\HomeController::class, 'europeanDistribution'])->name('european-distribution');

Route::get('/transport-management', [App\Http\Controllers\HomeController::class, 'transportManagement'])->name('transport-management');

Route::get('/fleet', [App\Http\Controllers\HomeController::class, 'fleet'])->name('fleet');

Route::get('/contact', [App\Http\Controllers\HomeController::class, 'contact'])->name('contact');

Route::get('/about', [App\Http\Controllers\HomeController::class, ''])->name('about');

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
