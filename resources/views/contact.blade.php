
@extends("layouts.main-layout")

@section("content")
<!-- Breadcrumb Area -->

<div class="breadcroumb-area bread-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcroumb-title">
                    <h1>Contact</h1>
                    <h6><a href="{{route("home")}}">Home</a> / Contact</h6>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Contact Area -->

<div id="contact-us" class="contact-us-area section-padding">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 text-center">
                <div class="section-title">
                    <h6>Contact Us</h6>
                    <h2>Don't hesitate to<b> contact</b> <br>for get <b>Information</b></h2>
                </div>
            </div>
        </div>
        <div class="contact-us-wrapper">
            <div class="row no-gutters">
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="contact-us-inner">
                        <div class="info-i"><span><i class="las la-map-marker"></i></span></div>
                        <h5>Office Location</h5>
                        <p> 43 Main Street Twickenham TW62 0TG <br>,UK</p>
                        <a href="#">Find us on map</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="contact-us-inner">
                        <div class="info-i"><span><i class="las la-clock"></i></span></div>
                        <h5>Office Hour</h5>
                        <p>Monday-Friday <br>08.00-20.00</p>
                        <a href="#">Get Direction</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="contact-us-inner">
                        <div class="info-i"><span><i class="las la-mobile"></i></span></div>
                        <h5>Phone Number</h5>
                        <p>+44 7405 239482
                        </p>
                        <a href="#">Call Now</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="contact-us-inner">
                        <div class="info-i"><span><i class="las la-envelope"></i></span></div>
                        <h5>E-mail Address</h5>
                        <p>info@jemshipping.com</p>
                        <a href="#">Mail Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Contact Form -->

<div id="contact-page" class="contact-section bg-cover section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 text-center wow fadeInRight">
                <div class="contact-form-wrapper mt-100">
                    <div class="section-title">
                        <h6>Stay with Us</h6>
                        <h2>Get in <b>Touch</b></h2>
                    </div>
                    <div class="contact-form">
                        <form action="">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="text" placeholder="Your Name">
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="email" placeholder="E-mail">
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="tel" placeholder="Phone Number">
                                </div>
                                <div class="col-lg-6 col-md-6 col-12">
                                    <input type="text" placeholder="Subject">
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="message" id="message" cols="30" rows="10" placeholder="Write Message"></textarea>
                                </div>
                                <div class="col-lg-12 col-md-6 col-12 text-center">
                                    <button class="main-btn">Get A Quote</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Client Area -->

<div class="client-area pt-50 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-carousel owl-carousel">
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/1.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/2.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/3.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/4.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/5.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/6.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/7.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
