@extends("layouts.main-layout")

@section("content")

<body>

<!-- Breadcroumb Area -->

<div class="breadcroumb-area bread-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcroumb-title">
                    <h1>Parcel Delivery Service</h1>
                    <h6><a href="{{route("home")}}">Home</a> / Parcel Delivery Service</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Service Details -->

<div id="service-page" class="service-details-section section-padding pb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
{{--                <div class="service-list">--}}
{{--                    <h5>Service Lists</h5>--}}
{{--                    <a href="single-service.html">Standard Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a href="single-service.html">Over Night Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a class="active" href="single-service.html">Express Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a href="single-service.html">Pallet Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a href="single-service.html">Warehouse Service<span><i class="las la-arrow-right"></i></span></a>--}}

{{--                </div>--}}

                <div class="question-section">
                    <h6>Have any Question?</h6>
                    <form action="">
                        <input type="text" name="name" id="name" required="" placeholder="Full Name">
                        <input type="email" name="email" id="email" required="" placeholder="Your E-mail">
                        <textarea name="message" id="message" cols="30" rows="10" required="" placeholder="How can help you?"></textarea>
                        <button class="btn btn-primary" type="submit">Your Question</button>
                    </form>
                </div>

                <div class="helpline-section">
                    <div class="helpline-content text-center">
                        <h4>Need Consultancy Help</h4>
                        <p>Gatherin galso sprit moving shall flow</p>
                        <button class="btn btn-primary" type="submit">Contact Us</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="single-service">
                    <img src="assets/img/service/single-service-bg.jpg" alt="">
                    <h2>Parcel Delivery Service</h2>
                    <p></p>
                    <hr>
                    <h5>Transforming brands with creativity</h5>
                    <p>As a direct touch point with end consumers,
                        it’s more important than ever to provide a premium quality parcel delivery service,
                        whilst still maintaining competitive pricing.

                        hat’s why so much time and effort went into choosing our primary parcel carrier – DPD.

                        DPD is the leading supplier of parcel delivery services in the UK and throughout the rest of the world.

                        When you chose the RCS Logistics and DPD partnership for your parcel delivery service, you’re choosing so much more.
                        With constant investment in technology to ensure your brand message carries right
                        through to delivery, to creating a parcel delivery experience, DPD can boast an unmatchable reputation for service and security.
                    </p>
{{--                    <div class="row">--}}
{{--                        <div class="col-lg-6 col-md-6 col-12">--}}
{{--                            <div class="key-feature">--}}
{{--                                <div class="row no-gutters">--}}
{{--                                    <div class="col-lg-4">--}}
{{--                                        <div class="about-icon">--}}
{{--                                            <img src="assets/img/icon/speed.png" alt="">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-12">--}}
{{--                                        <h4>Fast Delivery</h4>--}}
{{--                                        <p>knowledge of logistics rules better than anyone</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-6 col-md-6 col-12">--}}
{{--                            <div class="key-feature">--}}
{{--                                <div class="row no-gutters">--}}
{{--                                    <div class="col-lg-4">--}}
{{--                                        <div class="about-icon">--}}
{{--                                            <img src="assets/img/icon/shipping.png" alt="">--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-lg-12">--}}
{{--                                        <h4>Secured Services</h4>--}}
{{--                                        <p>knowledge of logistics rules better than anyone</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <h5>Courier Delivery Priorities</h5>--}}
{{--                    <p>Courier services compile several services for the delivery of portable goods, documents, and mail. As there is a wide range of products involved and each customer has different needs, courier companies offer several types of courier services. When planning to trust courier services to ship your goods, it is always a good idea to choose the service that meets your expectations. There are several services to choose from, and your choice should be based on several factors:</p>--}}
{{--                    <p>Express Courier Link is highly professional skilled & reliable Courier Service Provider Company which has been classified for courier delivery service for delivery.</p>--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="single-service-bg">--}}
{{--                                <img src="assets/img/service/service-bg-1.png" alt="">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="col-lg-6">--}}
{{--                            <div class="single-service-bg">--}}
{{--                                <img src="assets/img/service/service-bg-2.jpg" alt="">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>

<!--CTA Section-->

<div id="cta-2" class="cta-area">
    <div class="overlay-2"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-12">
                <h6>24/7 We Are Here</h6>
                <h2>Get a Free Quotation with our Expert</h2>
            </div>
            <div class="col-lg-6 offset-lg-1 col-md-4 text-right">
                <div class="contact-info">
                    <div class="main-btn small-btn">Make a Call</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Client Area -->

<div class="client-area pt-50 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-carousel owl-carousel">
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/1.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/2.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/3.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/4.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/5.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/6.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/7.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
