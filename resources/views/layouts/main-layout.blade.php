<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Jemshipping Logistics</title>

    <!--Favicon-->
    <link rel="icon" href="assets/img/favicon.png" type="image/jpg" />
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome CSS-->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Line Awesome CSS -->
    <link href="assets/css/line-awesome.min.css" rel="stylesheet">
    <!-- Animate CSS-->
    <link href="assets/css/animate.css" rel="stylesheet">
    <!-- Bar Filler CSS -->
    <link href="assets/css/barfiller.css" rel="stylesheet">
    <!-- Flaticon CSS -->
    <link href="assets/css/flaticon.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- jquery -->
    <script src="assets/js/jquery-1.12.4.min.js"></script>


</head>

<body>

<!-- Pre-Loader -->
<div id="loader">
    <div class="loading">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>

<!-- Header Top Area -->

<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="contact-info">
                    <i class="las la-envelope"></i> info@jemshipping.com |
                    <i class="las la-map-marker"></i>  43 Main Street Twickenham TW62 0TG, UK
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 text-right">
                <div class="site-info">
                    Turning big ideas into great products!
                    <div class="social-area">
                        <a href="#"><i class="lab la-facebook-f"></i></a>
                        <a href="#"><i class="lab la-instagram"></i></a>
                        <a href="#"><i class="lab la-twitter"></i></a>
                        <a href="#"><i class="la la-skype"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Header Area -->

<header class="header-area">
    <div class="sticky-area">
        <div class="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo">
{{--                            <a class="navbar-brand" href="index-2.html"><img src="assets/img/logo.png" alt=""></a>--}}
                            <a class="navbar-brand" href="{{route("home")}}"><img src="assets/img/jem_logo.png" alt=""></a>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="sub-title">
                            <p>Courier & Logistics Service</p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="main-menu">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                    <span class="navbar-toggler-icon"></span>
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route("home")}}">Home
                                            </a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route("about")}}">About Us</a>

                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route("fleet")}}">Fleet</a>

                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Logistics
                                                <span class="sub-nav-toggler"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="{{route("transport-logistics")}}">Transport Logistics</a></li>
                                                <li><a href="{{route("parcel-delivery")}}">Parcel Delivery Service</a></li>
                                                <li><a href="{{route("european-distribution")}}">European Distribution</a></li>
                                                <li><a href="{{route("transport-management")}}">Transport Management System</a></li>
                                            </ul>

                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="{{route("contact")}}">Contact</a>
                                        </li>
                                    </ul>

                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="header-right-content">
                            <div class="search-box">
                                <button class="search-btn"><i class="la la-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Dropdown Area -->

    <div class="search-popup">
        <span class="search-back-drop"></span>

        <div class="search-inner">
            <div class="auto-container">
                <div class="upper-text">
                    <div class="text">Search for anything.</div>
                    <button class="close-search"><span class="la la-times"></span></button>
                </div>

                <form method="post" action="http://capricorn-theme.net/html/expo/index.html">
                    <div class="form-group">
                        <input type="search" name="search-field" value="" placeholder="Search..." required="">
                        <button type="submit"><i class="la la-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>

@yield("content")


<!-- Footer Area -->

{{--<footer class="footer-area wow fadeIn" data-wow-delay=".2s">--}}
{{--    <div class="container">--}}
{{--        <div class="footer-up">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-12">--}}
{{--                    <div class="logo">--}}
{{--                        <img src="assets/img/logo-white.png" alt="expoint-logo">--}}
{{--                    </div>--}}
{{--                    <p>We are the Top Courier Service Provider and Logistics Solutions.--}}
{{--                        We provide services around the worldwide.</p>--}}
{{--                    <div class="social-area">--}}
{{--                        <a href="#"><i class="lab la-facebook-f"></i></a>--}}
{{--                        <a href="#"><i class="lab la-instagram"></i></a>--}}
{{--                        <a href="#"><i class="lab la-twitter"></i></a>--}}
{{--                        <a href="#"><i class="la la-skype"></i></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">--}}
{{--                    <h5>Explore</h5>--}}
{{--                    <ul>--}}
{{--                        <li>--}}
{{--                            <a href="#">About Company</a>--}}
{{--                            <a href="#">Latest News</a>--}}
{{--                            <a href="#">Get a Quote</a>--}}
{{--                            <a href="#">Pricing Guide</a>--}}
{{--                            <a href="#">Helpful FAQ</a>--}}
{{--                            <a href="#">Sitemap</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-12">--}}
{{--                    <h5>Services</h5>--}}
{{--                    <ul>--}}
{{--                        <li>--}}
{{--                            <a href="#"><span>-</span> Express Courier</a>--}}
{{--                            <a href="#"><span>-</span> Pallet Courier</a>--}}
{{--                            <a href="#"><span>-</span> Air Freight</a>--}}
{{--                            <a href="#"><span>-</span> Ocean Freight</a>--}}
{{--                            <a href="#"><span>-</span> Warehousing</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6">--}}
{{--                    <div class="subscribe-form">--}}
{{--                        <h5>Newsletter</h5>--}}
{{--                        <p>Sign Up now for latest news update</p>--}}
{{--                        <form action="http://capricorn-theme.net/html/expo/index.html">--}}
{{--                            <input type="email" placeholder="Your email">--}}
{{--                            <button class="main-btn">Subscribe</button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</footer>--}}

<div class="footer-bottom">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="copyright-line">© 2021 JemShipping. All rights reserved.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="privacy">Privacy Policy | Terms &amp; Conditions</p>
            </div>
        </div>
    </div>
</div>

<!-- Scroll Top Area -->
<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Wow JS -->
<script src="assets/js/wow.min.js"></script>
<!-- Way Points JS -->
<script src="assets/js/jquery.waypoints.min.js"></script>
<!-- Counter Up JS -->
<script src="assets/js/jquery.counterup.min.js"></script>
<!-- Owl Carousel JS -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Isotope JS -->
<script src="assets/js/isotope-3.0.6-min.js"></script>
<!-- Magnific Popup JS -->
<script src="assets/js/magnific-popup.min.js"></script>
<!-- Sticky JS -->
<script src="assets/js/jquery.sticky.js"></script>
<!-- Progress Bar JS -->
<script src="assets/js/jquery.barfiller.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>

@yield("scripts")

</body>


</html>
