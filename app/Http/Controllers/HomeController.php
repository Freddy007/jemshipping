<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function logistics()
    {
        return view('logistics');
    }

    public function transportLogistics()
    {
        return view('logistics.transport');
    }

    public function parcelDelivery()
    {
        return view('logistics.parcel-delivery');
    }

    public function europeanDistribution()
    {
        return view('logistics.european-distribution');
    }

    public function transportManagement()
    {
        return view('logistics.transport-management');
    }

    public function fleet()
    {
        return view('logistics.fleet');
    }

    public function aboutUs()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

}
