@extends("layouts.main-layout")

@section("content")

<!-- Breadcroumb Area -->

<div class="breadcroumb-area bread-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcroumb-title">
                    <h1>Fleet</h1>
                    <h6><a href="{{route("home")}}">Home</a> / Fleet</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Service Details -->

<div id="service-page" class="service-details-section section-padding pb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
{{--                <div class="service-list">--}}
{{--                    <h5>Service Lists</h5>--}}
{{--                    <a href="single-service.html">Standard Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a href="single-service.html">Over Night Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a class="active" href="single-service.html">Express Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a href="single-service.html">Pallet Courier<span><i class="las la-arrow-right"></i></span></a>--}}
{{--                    <a href="single-service.html">Warehouse Service<span><i class="las la-arrow-right"></i></span></a>--}}

{{--                </div>--}}

                <div class="question-section">
                    <h6>Have any Question?</h6>
                    <form action="http://capricorn-theme.net/html/expo/sendemail.php">
                        <input type="text" name="name" id="name" required="" placeholder="Full Name">
                        <input type="email" name="email" id="email" required="" placeholder="Your E-mail">
                        <textarea name="message" id="message" cols="30" rows="10" required="" placeholder="How can help you?"></textarea>
                        <button class="btn btn-primary" type="submit">Your Question</button>
                    </form>
                </div>

                <div class="helpline-section">
                    <div class="helpline-content text-center">
                        <h4>Need Consultancy Help</h4>
                        <p>Gatherin galso sprit moving shall flow</p>
                        <button class="btn btn-primary" type="submit">Contact Us</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="single-service">
{{--                    <img src="assets/img/service/single-service-bg.jpg" alt="">--}}
                    <img src="assets/img/fleet.jpg" alt="">
                    <h2>Our Fleet</h2>
                    <p>
                        We strive to ensure that our fleet of vehicles is not only diverse enough to cover any size consignment, but also that
                        it’s agile enough to offer our clients efficient and economic solutions.
                        All of our vehicles are fitted with state-of-the-art real-time GPS tracking technology, not only to ensure the safety of
                        our drivers, but also to give you an unrivaled service level,
                        rapid deliveries, and full access to data about your consignments.
                    </p>
                    <p>
                        The data stream from in-cab units is matched with online order entry and POD retrieval to provide
                        a complete, minute-by-minute picture of delivery status, meaning you’ll be able to track your delivers whenever you like.
                        The fleet consists of artics, rigids and vans, fitted with a variety of features, from units with
                        tail-lifts, to double-deck trailers and traditional curtain-siders.
                    </p>
                    <hr>
                    <h5>We have a Track Record</h5>
                    <p>Our drivers are highly experienced and fully vetted in all aspects of vehicle safety, and undergo
                        full in-house training and assessment on customer
                        service and becoming an ambassador for Jemshipping Logistics and your brand.
                        And it’s not just our drivers that are some of the best in the industry. Our dedicated transport team have over 120 years
                        combined experience, and their extensive knowledge ensures a smooth, reliable,
                        and on-time delivery service, not only when it matters, but every time.
                    </p>

                    <h5>We Invest in Our Fleets</h5>
                    <p>Recent years have seen endless technological developments in the automotive industry.
                        That's why we continually invest in our fleet, whether that means expanding our range of vehicles,
                        installing the latest telematics systems, or exploring new environmentally-friendly solutions.
                    </p>
                    <p>
                        Express Courier Link is highly professional skilled & reliable Courier Service Provider Company
                        which has been classified for courier delivery service for delivery.
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>

<!--CTA Section-->

<div id="cta-2" class="cta-area">
    <div class="overlay-2"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-12">
                <h6>24/7 We Are Here</h6>
                <h2>Get a Free Quotation with our Expert</h2>
            </div>
            <div class="col-lg-6 offset-lg-1 col-md-4 text-right">
                <div class="contact-info">
                    <div class="main-btn small-btn">Make a Call</div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
