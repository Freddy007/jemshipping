@extends("layouts.main-layout")

@section("content")

<body>

<!-- Breadcroumb Area -->

<div class="breadcroumb-area bread-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcroumb-title">
                    <h1>Transport Logistics</h1>
                    <h6><a href="{{route("home")}}">Home</a> / Transport Logistics</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Service Details -->

<div id="service-page" class="service-details-section section-padding pb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="question-section">
                    <h6>Have any Question?</h6>
                    <form action="http://capricorn-theme.net/html/expo/sendemail.php">
                        <input type="text" name="name" id="name" required="" placeholder="Full Name">
                        <input type="email" name="email" id="email" required="" placeholder="Your E-mail">
                        <textarea name="message" id="message" cols="30" rows="10" required="" placeholder="How can help you?"></textarea>
                        <button class="btn btn-primary" type="submit">Your Question</button>
                    </form>
                </div>

                <div class="helpline-section">
                    <div class="helpline-content text-center">
                        <h4>Need Consultancy Help</h4>
                        <p>Gatherin galso sprit moving shall flow</p>
                        <button class="btn btn-primary" type="submit">Contact Us</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="single-service">
{{--                    <img src="assets/img/service/single-service-bg.jpg" alt="">--}}
                    <img src="assets/img/transport-logistics.jpg" alt="">
                    <h2>  Reliable UK & Ireland Transport Logistics</h2>
                    <p>

                        Because we understand that your top priority is to get your goods to your customers on time and in full, we offer a full spectrum of transport logistics
                        solutions to ensure you have the flexibility to send different sizes of consignment without having to find a new provider.

                    </p>
                    <p>
                        With so many options available you can rest assured that we will be able to deliver your consignment,
                        regardless of its size. And if there’s ever a time where you need some advice on choosing the right solution,
                        our transport team, who have more than 120 years’
                        experience between them, will be happy to help.

                    </p>
                    <hr>
                    <h5>Transforming brands with creativity</h5>
                    <p>

                        Full, Part, and Consolidated Loads
                        Our dedicated fleet of vehicles operates nationally throughout the UK delivering both full, part, and consolidated loads. To get a ‘Quick Quote’ simply complete this form and a member of our team will be in touch with you shortly.

                        Palletforce
                        Sending smaller consignments of less than 10 pallets used to be expensive business, but we have a solution for you…Palletforce – the leading pallet network in the UK. With over 88 members in the UK alone, there’s hundreds of experts making sure your pallet gets from A to B safely – and that’s not including their European services.

                        European Transport Logistics
                        In addition to our UK services, through our trusted and fully-vetted network of partners, we offer a full import and export service covering the whole of Europe.

                    </p>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="key-feature">
                                <div class="row no-gutters">
                                    <div class="col-lg-4">
                                        <div class="about-icon">
{{--                                            <img src="assets/img/icon/speed.png" alt="">--}}
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
{{--                                        <h4>Fast Delivery</h4>--}}
{{--                                        <p>knowledge of logistics rules better than anyone</p>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="key-feature">
                                <div class="row no-gutters">
                                    <div class="col-lg-4">
                                        <div class="about-icon">
{{--                                            <img src="assets/img/icon/shipping.png" alt="">--}}
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
{{--                                        <h4>Secured Services</h4>--}}
{{--                                        <p>knowledge of logistics rules better than anyone</p>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h5>Courier Delivery Priorities</h5>
                    <p>Courier services compile several services for the delivery of portable goods, documents, and mail. As there is a wide range of products involved and each customer has different needs, courier companies offer several types of courier services. When planning to trust courier services to ship your goods, it is always a good idea to choose the service that meets your expectations. There are several services to choose from, and your choice should be based on several factors:</p>
                    <p>Express Courier Link is highly professional skilled & reliable Courier Service Provider Company which has been classified for courier delivery service for delivery.</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="single-service-bg">
                                <img src="assets/img/service/service-bg-1.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single-service-bg">
                                <img src="assets/img/service/service-bg-2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--CTA Section-->

<div id="cta-2" class="cta-area">
    <div class="overlay-2"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-8 col-12">
                <h6>24/7 We Are Here</h6>
                <h2>Get a Free Quotation with our Expert</h2>
            </div>
            <div class="col-lg-6 offset-lg-1 col-md-4 text-right">
                <div class="contact-info">
                    <div class="main-btn small-btn">Make a Call</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Client Area -->

<div class="client-area pt-50 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-carousel owl-carousel">
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/1.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/2.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/3.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/4.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/5.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/6.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/7.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
