<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> Expoint | Courier &amp; Logistics HTML Template</title>

    <!--Favicon-->
    <link rel="icon" href="assets/img/favicon.png" type="image/jpg" />
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome CSS-->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Line Awesome CSS -->
    <link href="assets/css/line-awesome.min.css" rel="stylesheet">
    <!-- Animate CSS-->
    <link href="assets/css/animate.css" rel="stylesheet">
    <!-- Bar Filler CSS -->
    <link href="assets/css/barfiller.css" rel="stylesheet">
    <!-- Flaticon CSS -->
    <link href="assets/css/flaticon.css" rel="stylesheet">
    <!-- Owl Carousel CSS -->
    <link href="assets/css/owl.carousel.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- Responsive CSS -->
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- jquery -->
    <script src="assets/js/jquery-1.12.4.min.js"></script>


</head>

<body>

<!-- Pre-Loader -->
<div id="loader">
    <div class="loading">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>

<!-- Header Top Area -->

<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="contact-info">
                    <i class="las la-envelope"></i> info@expoint.com |
                    <i class="las la-map-marker"></i> 66, Broklyn St, New York, USA
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 text-right">
                <div class="site-info">
                    Turning big ideas into great products!
                    <div class="social-area">
                        <a href="#"><i class="lab la-facebook-f"></i></a>
                        <a href="#"><i class="lab la-instagram"></i></a>
                        <a href="#"><i class="lab la-twitter"></i></a>
                        <a href="#"><i class="la la-skype"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Header Area -->

<header class="header-area">
    <div class="sticky-area">
        <div class="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="logo">
                            <a class="navbar-brand" href="index-2.html"><img src="assets/img/logo.png" alt=""></a>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="sub-title">
                            <p>Courier & Logistics Service</p>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="main-menu">
                            <nav class="navbar navbar-expand-lg">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                    <span class="navbar-toggler-icon"></span>
                                    <span class="navbar-toggler-icon"></span>
                                </button>

                                <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#">Home
                                                <span class="sub-nav-toggler">
 													</span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="index-2.html">Home- Main</a></li>
                                                <li><a href="index-3.html">Home- Courier</a></li>
                                                <li><a href="index-4.html">Home- Logistics</a></li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Pages
                                                <span class="sub-nav-toggler">
 													</span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="about.html">About us</a></li>
                                                <li><a href="choose-us.html">Why Choose Us</a></li>
                                                <li><a href="team.html">Our Team</a></li>
                                                <li><a href="price.html">Pricing</a></li>
                                                <li><a href="quote.html">Quotation</a></li>
                                                <li><a href="faq.html">FAQ</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Services
                                                <span class="sub-nav-toggler">
 													</span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="services-1.html">Services-01</a></li>
                                                <li><a href="services-2.html">Services-02</a></li>
                                                <li><a href="single-service.html">Service Details</a></li>
                                            </ul>

                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Freight
                                                <span class="sub-nav-toggler">
 													</span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="air-freight.html">Air Freight</a></li>
                                                <li><a href="sea-freight.html">Sea Freight</a></li>
                                                <li><a href="ground-freight.html">Ground Freight</a></li>
                                                <li><a href="rail-freight.html">Rail Freight</a></li>
                                            </ul>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Blog
                                                <span class="sub-nav-toggler">
 													</span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="blog-classic.html">Blog-Classic</a></li>
                                                <li><a href="blog-grid.html">Blog-Grid</a></li>
                                                <li><a href="single-blog.html">Blog Details</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.html">Contact</a>
                                        </li>
                                    </ul>

                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="header-right-content">
                            <div class="search-box">
                                <button class="search-btn"><i class="la la-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Dropdown Area -->

    <div class="search-popup">
        <span class="search-back-drop"></span>

        <div class="search-inner">
            <div class="auto-container">
                <div class="upper-text">
                    <div class="text">Search for anything.</div>
                    <button class="close-search"><span class="la la-times"></span></button>
                </div>

                <form method="post" action="http://capricorn-theme.net/html/expo/index.html">
                    <div class="form-group">
                        <input type="search" name="search-field" value="" placeholder="Search..." required="">
                        <button type="submit"><i class="la la-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>

<!-- Hero Area -->

<div class="homepage-slides owl-carousel">
    <div class="single-slide-item">
        <div class="overlay"></div>
        <div class="hero-area-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 wow fadeInUp animated" data-wow-delay=".3s">
                        <div class="section-title">
                            <h6>Welcome to Expoint</h6>
                            <h1>We are experts in<br> <b>Global Courier</b></h1>
                            <p> Logistics Revolution, in modern history, the process of change from an agrarian <br>and handicraft economy to one dominated by courier and deliver services.</p>
                        </div>
                        <a href="about.html" class="main-btn">Get A Quote</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-slide-item hero-area-bg-2">
        <div class="overlay"></div>
        <div class="hero-area-content">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 wow fadeInUp animated" data-wow-delay=".3s">
                        <div class="section-title">
                            <h6>Since in 1995</h6>
                            <h1>Best Solutions for<br> <b>Delivery</b> Services</h1>
                            <p> Logistics Revolution, in modern history, the process of change from an agrarian <br>and handicraft economy to one dominated by courier and deliver services.</p>
                        </div>
                        <a href="quote.html" class="main-btn">Get A Quote</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Feature Section -->

<div class="feature-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-feature-item">
                    <div class="row no-gutters">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div class="feat-icon">
                                <img src="assets/img/icon/business.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-12">
                            <div class="feat-content">
                                <h5>Flat Rate Fees</h5>
                                <p>It is a long established fact that and reader will by the readable based.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-feature-item">
                    <div class="row no-gutters">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div class="feat-icon">
                                <img src="assets/img/icon/24-hours.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-12">
                            <div class="feat-content">
                                <h5>24/7 Services</h5>
                                <p>It is a long established fact that and reader will by the readable based.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12">
                <div class="single-feature-item">
                    <div class="row no-gutters">
                        <div class="col-lg-4 col-md-12 col-12">
                            <div class="feat-icon">
                                <img src="assets/img/icon/bounce-rate.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 col-12">
                            <div class="feat-content">
                                <h5>Free Estimate</h5>
                                <p>It is a long established fact that and reader will by the readable based.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- About Section-->

<div class="about-area section-padding pb-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 wow fadeInLeft" data-wow-delay=".3s">
                <div class="info-content-area">
                    <div class="section-title">
                        <h6>About Us</h6>
                        <h2>We're leading <b>Courier <br>Service</b> in Worldwide</h2>
                    </div>
                    <p>We are serve worldwide site lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea enim et, similique, minus soluta ducimus.</p>
                    <p class="highlight">Since our launch in 1995, to deliver high value package.</p>
                    <p class="highlight">We always provide flexible & quality task. </p>
                    <p class="highlight">Unique latest machinary used the Logistics project.</p>

                    <div class="row founded">
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="years"><span>25</span>Year of Success</div>
                        </div>
                        <div class="col-lg-8 col-md-6 col-12">
                            <div class="text">Since we established in 1995 experience &amp; still a growing protfolio day by day!</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="info-img">
                    <img src="assets/img/about/about-men.png" alt="">
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Services Area -->

<div id="service-1" class="services-area bg-cover section-padding">
    <div class="overlay-2"></div>
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 text-center">
                <div class="section-title">
                    <h6>Services</h6>
                    <h2>We Provide Various Category <br><b>Delivery Services</b></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-service-area mt-30 mb-50 wow fadeInLeft" data-wow-delay=".2s">
                    <div class="service-icon">
                        <i class="flaticon-delivery-man"></i>
                    </div>
                    <h4>Standard Courier</h4>
                    <p>This services involve transferring the parcels to the closest depot to the delivery location.</p>
                    <a href="single-service.html" class="read-more">Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-service-area service-2 mt-30 mb-50 wow fadeInLeft" data-wow-delay=".4s">
                    <div class="service-icon">
                        <i class="flaticon-truck"></i>
                    </div>
                    <h4>Express Courier</h4>
                    <p>This is a service provided to those who need urgent delivery to be sent and received on the same day.</p>
                    <a href="single-service.html" class="read-more">Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-service-area service-3 mt-30 mb-50 wow fadeInLeft" data-wow-delay=".6s">
                    <div class="service-icon">
                        <i class="flaticon-pallet"></i>
                    </div>
                    <h4>Pallet Courier</h4>
                    <p>This is a service that safely strict delivery and promptly delivers goods on pallets.</p>
                    <a href="single-service.html" class="read-more">Read More</a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-service-area service-4 mt-30 wow fadeInLeft" data-wow-delay=".2s">
                    <div class="service-icon">
                        <i class="flaticon-fast-delivery"></i>
                    </div>
                    <h4>Over Night Courier</h4>
                    <p>This courier will usually transport the goods during the night or early of the morning.</p>
                    <a href="single-service.html" class="read-more">Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-service-area service-5 mt-30 wow fadeInLeft" data-wow-delay=".4s">
                    <div class="service-icon">
                        <i class="flaticon-air-freight"></i>
                    </div>
                    <h4>International Courier</h4>
                    <p>This is a transport service of goods or documents from one country to another country.</p>
                    <a href="single-service.html" class="read-more">Read More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-service-area service-6 mt-30 wow fadeInLeft" data-wow-delay=".6s">
                    <div class="service-icon">
                        <i class="flaticon-wholesale"></i>
                    </div>
                    <h4>Warehousing</h4>
                    <p>This type of service will involve managed storage solutions to give companies greater control.</p>
                    <a href="single-service.html" class="read-more">Read More</a>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Logistics Section -->

<div id="logistic-1" class="logistic-area sky-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center ">
                <div class="section-title">
                    <h6>Favourite Consignment</h6>
                    <h2>Secured, Affordable & Reliable <br> <b>Logistics Support</b></h2>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="single-logistic-wrapper mt-40">
                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <div class="single-logistic-bg bg-cover">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single-logistic-area">
                                <div class="service-icon">
                                    <i class="flaticon-air-freight"></i>
                                </div>
                                <h5>Air Freight</h5>
                                <p>There are some reason build the site lorem ipsum dolor sit amet.</p>
                                <a href="single-service.html" class="read-more">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="single-logistic-wrapper mt-40">
                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <div class="single-logistic-bg logistic-bg-2 bg-cover">
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="single-logistic-area">
                                <div class="service-icon">
                                    <i class="flaticon-vessel"></i>
                                </div>
                                <h5>Sea Freight</h5>
                                <p>There are some reason build the site lorem ipsum dolor sit amet.</p>
                                <a href="single-service.html" class="read-more">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="single-logistic-wrapper">
                    <div class="row no-gutters">

                        <div class="col-lg-6">
                            <div class="single-logistic-area">
                                <div class="service-icon">
                                    <i class="flaticon-truck"></i>
                                </div>
                                <h5>Ground Freight</h5>
                                <p>There are some reason build the site lorem ipsum dolor sit amet.</p>
                                <a href="single-service.html" class="read-more">Read More</a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single-logistic-bg logistic-bg-3 bg-cover">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="single-logistic-wrapper">
                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <div class="single-logistic-area">
                                <div class="service-icon">
                                    <i class="flaticon-pallet"></i>
                                </div>
                                <h5>Warehouseing</h5>
                                <p>There are some reason build the site lorem ipsum dolor sit amet.</p>
                                <a href="single-service.html" class="read-more">Read More</a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single-logistic-bg logistic-bg-4 bg-cover">
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Process Area -->

<div class="process-area section-padding">
    <div class="container">
        <div class="row">
            <div class="offset-lg-2 col-lg-8 text-center">
                <div class="section-title">
                    <h6>Our Process</h6>
                    <h2>How We <b>Work</b> for <b>Customers</b></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="single-process-area wow fadeInLeft" data-wow-delay=".2s">
                    <div class="process-icon">
                        <img src="assets/img/icon/web.png" alt="">
                    </div>
                    <h4>Apply Online</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="single-process-area wow fadeInLeft" data-wow-delay=".4s">
                    <div class="process-icon">
                        <img src="assets/img/icon/document.png" alt="">
                    </div>
                    <h4>Documentation</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay=".6s">
                <div class="single-process-area">
                    <div class="process-icon">
                        <img src="assets/img/icon/call-center.png" alt="">
                    </div>
                    <h4>Processing</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="single-process-area wow fadeInLeft" data-wow-delay=".8s">
                    <div class="process-icon">
                        <img src="assets/img/icon/package.png" alt="">
                    </div>
                    <h4>Final Destination</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Quotation Section-->

<div class="quotation-section bg-cover section-padding">
    <div class="overlay-2"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 text-center">
                <div class="section-title">
                    <h6>Quotation</h6>
                    <h2>Get Free<b> Quotation</b> for <br>Your <b>Choice</b></h2>
                </div>
            </div>
        </div>
        <div class="quotation-block wow fadeInUp" data-wow-delay=".3s">
            <form class="quotation-form" method="post">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Full Name" required>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="phone">Phone No.</label>
                            <input type="text" class="form-control" id="phone" placeholder="Phone No." required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label>Select Courier Type</label>
                            <div class="form-group">
                                <select class="form-control" id="courier-type-box">
                                    <option>Standard</option>
                                    <option>Express</option>
                                    <option>International</option>
                                    <option>Pallet</option>
                                    <option>Warehousing</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Select Location</label>
                            <div class="form-group">
                                <div class="row no-gutters">
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="From" required>
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" placeholder="To" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="form-group">
                            <label>Select Freight</label>
                            <div class="form-group">
                                <select class="form-control">
                                    <option>Air Freight</option>
                                    <option>Sea Freight</option>
                                    <option>Ground Freight</option>
                                    <option>Rail Freight</option>
                                    <option>Warehousing</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="request-button">
                            <button type="submit" class="main-btn">Send Request<i class="las la-arrow-right"></i></button>
                        </div>
                    </div>
                </div>

            </form>
            <div class="quotation-dtl text-white">
                <p><i class="las la-mobile"></i>We are available at Mon-Fri call us + 212-4000-300 during regular business hours</p>
            </div>
        </div>
    </div>
</div>

<!-- FAQ Section  -->

<div class="faq-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 wow fadeInLeft" data-wow-delay=".3s">
                <div class="faq-bg bg-cover">
                </div>
            </div>
            <div class="col-lg-5 mt-40 wow fadeInRight" data-wow-delay=".4s">
                <div class="section-title">
                    <h6>Helpful FAQ's</h6>
                    <h2>Frequently Asked <b>Questions</b></h2>
                </div>
                <div class="styled-faq">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading active" role="tab" id="headingOne">
                                <h6 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        When can I expect my delivery?
                                        <i class="fa fa-angle-up"></i>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </h6>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        What is the first possible pick-up date?
                                        <i class="fa fa-angle-up"></i>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </h6>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        How can I pay for the service?
                                        <i class="fa fa-angle-up"></i>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </h6>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch.
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading active" role="tab" id="headingSeven">
                                <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                        How can I collect my parcel in the parcel shop?
                                        <i class="fa fa-angle-up"></i>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                </h6>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Testimonial Section -->

<div class="testimonial-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="testimonial-carousel owl-carousel">
                    <div class="single-testimonial-item">
                        <div class="testimonial-avatar">
                            <img src="assets/img/testimonial/2.jpg" alt="">
                        </div>
                        <div class="testimonial-content">
                            <div class="testimonial-icon-before"><i class="las la-quote-left"></i></div>
                            <p>There are some reason lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, officia eligendi quos best choice similique autem laborum repellendus aliquam, support all molestiae tempora quod esse sed. amazing service.</p>
                            <div class="testimonial-icon-after"><i class="las la-quote-right"></i></div>
                            <h4>Mihir Kanti Ghosh <span>CEO &amp; Founder-Linking Park</span></h4>
                        </div>
                    </div>

                    <div class="single-testimonial-item">
                        <div class="testimonial-avatar">
                            <img src="assets/img/testimonial/4.jpg" alt="">
                        </div>
                        <div class="testimonial-content">
                            <div class="testimonial-icon-before"><i class="las la-quote-left"></i></div>
                            <p>There are some reason lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, officia eligendi quos best choice similique autem laborum repellendus aliquam, support all molestiae tempora quod esse sed. amazing service.</p>
                            <div class="testimonial-icon-after"><i class="las la-quote-right"></i></div>
                            <h4>Mihir Kanti Ghosh <span>CEO &amp; Founder-Linking Park</span></h4>
                        </div>
                    </div>

                    <div class="single-testimonial-item">
                        <div class="testimonial-avatar">
                            <img src="assets/img/testimonial/3.jpg" alt="">
                        </div>
                        <div class="testimonial-content">
                            <div class="testimonial-icon-before"><i class="las la-quote-left"></i></div>
                            <p>There are some reason lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, officia eligendi quos best choice similique autem laborum repellendus aliquam, support all molestiae tempora quod esse sed. amazing service.</p>
                            <div class="testimonial-icon-after"><i class="las la-quote-right"></i></div>
                            <h4>Mihir Kanti Ghosh <span>CEO &amp; Founder-Linking Park</span></h4>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- Blog Section-->

<div class="blog-area gray-bg section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <div class="section-title">
                    <h6>Blog</h6>
                    <h2>What <b>happen</b> inside our <b>company</b></h2>
                </div>
            </div>

            <div class="col-lg-6 text-right">

            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-blog-item wow fadeInLeft" data-wow-delay=".4s">
                    <div class="blog-bg">
                        <img src="assets/img/blog/1.jpg" alt="">
                    </div>
                    <div class="blog-content">
                        <p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>25 Feb</p>
                        <h5><a href="single-blog.html">Five Importance logistics solutions for Delivery Services </a>
                        </h5>
                        <p>There are some reason Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, facilis perferendis ipsam.</p>
                        <a href="single-blog.html" class="read-more">Read More</a>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-blog-item wow fadeInLeft" data-wow-delay=".6s">
                    <div class="blog-bg">
                        <img src="assets/img/blog/2.jpg" alt="">
                    </div>
                    <div class="blog-content">
                        <p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>16 Jan</p>
                        <h5><a href="single-blog.html">Cargo Shipment on Ocean Freight Demands Costly</a></h5>
                        <p>There are some reason Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, facilis perferendis ipsam.</p>
                        <a href="single-blog.html" class="read-more">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="single-blog-item wow fadeInLeft" data-wow-delay=".8s">
                    <div class="blog-bg">
                        <img src="assets/img/blog/3.jpg" alt="">
                    </div>
                    <div class="blog-content">
                        <p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>01 Jan</p>
                        <h5><a href="single-blog.html">We believe in building long lasting our business relationships</a></h5>
                        <p>There are some reason Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, facilis perferendis ipsam.</p>
                        <a href="single-blog.html" class="read-more">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Client Area -->

<div class="client-area pt-50 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-carousel owl-carousel">
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/1.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/2.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/3.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/4.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/5.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/6.png" alt="">
                        </div>
                    </div>
                    <div class="single-logo-wrapper">
                        <div class="logo-inner-item">
                            <img src="assets/img/client/7.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer Area -->

<footer class="footer-area wow fadeIn" data-wow-delay=".2s">
    <div class="container">
        <div class="footer-up">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="logo">
                        <img src="assets/img/logo-white.png" alt="expoint-logo">
                    </div>
                    <p>We are the Top Courier Service Provider and Logistics Solutions. We’re services around over the worldwide. We never give up on the challenges.</p>
                    <div class="social-area">
                        <a href="#"><i class="lab la-facebook-f"></i></a>
                        <a href="#"><i class="lab la-instagram"></i></a>
                        <a href="#"><i class="lab la-twitter"></i></a>
                        <a href="#"><i class="la la-skype"></i></a>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-6 com-sm-12">
                    <h5>Explore</h5>
                    <ul>
                        <li>
                            <a href="#">About Company</a>
                            <a href="#">Latest News</a>
                            <a href="#">Get a Quote</a>
                            <a href="#">Pricing Guide</a>
                            <a href="#">Helpful FAQ</a>
                            <a href="#">Sitemap</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <h5>Services</h5>
                    <ul>
                        <li>
                            <a href="#"><span>-</span> Express Courier</a>
                            <a href="#"><span>-</span> Pallet Courier</a>
                            <a href="#"><span>-</span> Air Freight</a>
                            <a href="#"><span>-</span> Ocean Freight</a>
                            <a href="#"><span>-</span> Warehousing</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="subscribe-form">
                        <h5>Newsletter</h5>
                        <p>Sign Up now for latest news update</p>
                        <form action="http://capricorn-theme.net/html/expo/index.html">
                            <input type="email" placeholder="Your email">
                            <button class="main-btn">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="footer-bottom">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="copyright-line">© 2021 Expoint. All rights reserved.</p>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <p class="privacy">Privacy Policy | Terms &amp; Conditions</p>
            </div>
        </div>
    </div>
</div>

<!-- Scroll Top Area -->
<a href="#top" class="go-top"><i class="las la-angle-up"></i></a>


<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Wow JS -->
<script src="assets/js/wow.min.js"></script>
<!-- Way Points JS -->
<script src="assets/js/jquery.waypoints.min.js"></script>
<!-- Counter Up JS -->
<script src="assets/js/jquery.counterup.min.js"></script>
<!-- Owl Carousel JS -->
<script src="assets/js/owl.carousel.min.js"></script>
<!-- Isotope JS -->
<script src="assets/js/isotope-3.0.6-min.js"></script>
<!-- Magnific Popup JS -->
<script src="assets/js/magnific-popup.min.js"></script>
<!-- Sticky JS -->
<script src="assets/js/jquery.sticky.js"></script>
<!-- Progress Bar JS -->
<script src="assets/js/jquery.barfiller.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>

</body>


<!-- Mirrored from capricorn-theme.net/html/expo/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 31 Jan 2021 12:38:15 GMT -->
</html>
