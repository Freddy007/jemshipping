@extends("layouts.main-layout")

@section("content")

    <!-- Hero Area -->

    <div class="homepage-slides owl-carousel">
        <div class="single-slide-item">
            <div class="overlay"></div>
            <div class="hero-area-content">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 wow fadeInUp animated" data-wow-delay=".3s">
                            <div class="section-title">
                                <h6>Welcome to Jemshipping</h6>
                                <h1>We are experts in<br> <b>Global Courier</b></h1>
                                <p>
                                    Global locations ensure that you have a dedicated partner in every port
                                </p>
                            </div>
                            <a href="" class="main-btn">Get A Quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="single-slide-item hero-area-bg-2">
            <div class="overlay"></div>
            <div class="hero-area-content">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-12 wow fadeInUp animated" data-wow-delay=".3s">
                            <div class="section-title">
                                <h6>Since in 1995</h6>
                                <h1>Best Solutions for<br> <b>Delivery</b> Services</h1>
                                <p>
                                    Providing modern supply chain solutions built on a foundation of integrity and trust.
                                </p>
                            </div>
                            <a href="/" class="main-btn">Get A Quote</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Feature Section -->

    <div class="feature-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-feature-item">
                        <div class="row no-gutters">
                            <div class="col-lg-4 col-md-12 col-12">
                                <div class="feat-icon">
                                    <img src="assets/img/icon/business.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12 col-12">
                                <div class="feat-content">
                                    <h5>Flat Rate Fees</h5>
                                    <p>We have flat rate across all services provided by Jemshipping. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-feature-item">
                        <div class="row no-gutters">
                            <div class="col-lg-4 col-md-12 col-12">
                                <div class="feat-icon">
                                    <img src="assets/img/icon/24-hours.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12 col-12">
                                <div class="feat-content">
                                    <h5>24/7 Services</h5>
                                    <p>Our Customer service agents are available 24/7 to assist you.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="single-feature-item">
                        <div class="row no-gutters">
                            <div class="col-lg-4 col-md-12 col-12">
                                <div class="feat-icon">
                                    <img src="assets/img/icon/bounce-rate.png" alt="">
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-12 col-12">
                                <div class="feat-content">
                                    <h5>Free Estimate</h5>
                                    <p>We provide estimates for a services for free of charge.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- About Section-->

    <div class="about-area section-padding pb-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 wow fadeInLeft" data-wow-delay=".3s">
                    <div class="info-content-area">
                        <div class="section-title">
                            <h6>About Us</h6>
                            <h2>We're leading <b>Courier <br>Service</b> Worldwide</h2>
                        </div>
{{--                        <p>We are serve worldwide site lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea enim et, similique, minus soluta ducimus.</p>--}}
                        <p class="highlight">Since our launch in 1995, to deliver high value package.</p>
                        <p class="highlight">We always provide flexible & quality Service. </p>
                        <p class="highlight">latest machinery used in Logistics project.</p>

                        <div class="row founded">
                            <div class="col-lg-4 col-md-6 col-12">
                                <div class="years"><span>25</span>Year of Success</div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-12">
                                <div class="text">Since we established in 1995 experience &amp; still a growing protfolio day by day!</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12 wow fadeInUp" data-wow-delay=".4s">
                    <div class="info-img">
                        <img src="assets/img/about/about-men.png" alt="">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Services Area -->

    <div id="service-1" class="services-area bg-cover section-padding">
        <div class="overlay-2"></div>
        <div class="container">
            <div class="row">
                <div class="offset-lg-2 col-lg-8 text-center">
                    <div class="section-title">
                        <h6>Services</h6>
                        <h2>We Provide Various Category <br><b>Delivery Services</b></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="single-service-area mt-30 mb-50 wow fadeInLeft" data-wow-delay=".2s">
                        <div class="service-icon">
                            <i class="flaticon-delivery-man"></i>
                        </div>
                        <h4>Standard Courier</h4>
                        <p>This services involve transferring the parcels to the closest depot to the delivery location.</p>
                        <a href="single-service.html" class="read-more">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="single-service-area service-2 mt-30 mb-50 wow fadeInLeft" data-wow-delay=".4s">
                        <div class="service-icon">
                            <i class="flaticon-truck"></i>
                        </div>
                        <h4>Express Courier</h4>
                        <p>This is a service provided to those who need urgent delivery to be sent and received on the same day.</p>
                        <a href="single-service.html" class="read-more">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="single-service-area service-3 mt-30 mb-50 wow fadeInLeft" data-wow-delay=".6s">
                        <div class="service-icon">
                            <i class="flaticon-pallet"></i>
                        </div>
                        <h4>Pallet Courier</h4>
                        <p>This is a service that safely strict delivery and promptly delivers goods on pallets.</p>
                        <a href="single-service.html" class="read-more">Read More</a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="single-service-area service-4 mt-30 wow fadeInLeft" data-wow-delay=".2s">
                        <div class="service-icon">
                            <i class="flaticon-fast-delivery"></i>
                        </div>
                        <h4>Over Night Courier</h4>
                        <p>This courier will usually transport the goods during the night or early of the morning.</p>
                        <a href="single-service.html" class="read-more">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="single-service-area service-5 mt-30 wow fadeInLeft" data-wow-delay=".4s">
                        <div class="service-icon">
                            <i class="flaticon-air-freight"></i>
                        </div>
                        <h4>International Courier</h4>
                        <p>This is a transport service of goods or documents from one country to another country.</p>
                        <a href="single-service.html" class="read-more">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="single-service-area service-6 mt-30 wow fadeInLeft" data-wow-delay=".6s">
                        <div class="service-icon">
                            <i class="flaticon-wholesale"></i>
                        </div>
                        <h4>Warehousing</h4>
                        <p>This type of service will involve managed storage solutions to give companies greater control.</p>
                        <a href="single-service.html" class="read-more">Read More</a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Logistics Section -->

    <div id="logistic-1" class="logistic-area sky-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center ">
                    <div class="section-title">
                        <h6>Favourite Consignment</h6>
                        <h2>Secured, Affordable & Reliable <br> <b>Logistics Support</b></h2>
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="single-logistic-wrapper mt-40">
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="single-logistic-bg bg-cover">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-logistic-area">
                                    <div class="service-icon">
                                        <i class="flaticon-air-freight"></i>
                                    </div>
                                    <h5>Air Freight</h5>
                                    <p>
{{--                                        There are some reason build the site lorem ipsum dolor sit amet.--}}
                                        We provide fast and efficient air fright shipping services to your destination of choice.
                                    </p>
{{--                                    <a href="single-service.html" class="read-more">Read More</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="single-logistic-wrapper mt-40">
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="single-logistic-bg logistic-bg-2 bg-cover">
                                </div>

                            </div>
                            <div class="col-lg-6">
                                <div class="single-logistic-area">
                                    <div class="service-icon">
                                        <i class="flaticon-vessel"></i>
                                    </div>
                                    <h5>Sea Freight</h5>
                                    <p>
                                        Reap the benefits of worldwide capacity, reliable sailing times
                                        and competitive rates to get your cargo where it needs to be,
                                        when it needs to be there.

                                    </p>
{{--                                    <a href="single-service.html" class="read-more">Read More</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="single-logistic-wrapper">
                        <div class="row no-gutters">

                            <div class="col-lg-6">
                                <div class="single-logistic-area">
                                    <div class="service-icon">
                                        <i class="flaticon-truck"></i>
                                    </div>
                                    <h5>Ground Freight</h5>
                                    <p>
                                        We offer ground freight shipping services for goods that require an average
                                        transit time of three to 10 days, depending on the pickup and delivery locations.
                                    </p>
{{--                                    <a href="single-service.html" class="read-more">Read More</a>--}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-logistic-bg logistic-bg-3 bg-cover">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="single-logistic-wrapper">
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="single-logistic-area">
                                    <div class="service-icon">
                                        <i class="flaticon-pallet"></i>
                                    </div>
                                    <h5>Warehousing</h5>
                                    <p>
                                        We provide both domestic and
                                        international warehousing and distribution services for our shipping clients and partners
                                    </p>
{{--                                    <a href="single-service.html" class="read-more">Read More</a>--}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single-logistic-bg logistic-bg-4 bg-cover">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Process Area -->

{{--    <div class="process-area section-padding">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="offset-lg-2 col-lg-8 text-center">--}}
{{--                    <div class="section-title">--}}
{{--                        <h6>Our Process</h6>--}}
{{--                        <h2>How We <b>Work</b> for <b>Customers</b></h2>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-12">--}}
{{--                    <div class="single-process-area wow fadeInLeft" data-wow-delay=".2s">--}}
{{--                        <div class="process-icon">--}}
{{--                            <img src="assets/img/icon/web.png" alt="">--}}
{{--                        </div>--}}
{{--                        <h4>Apply Online</h4>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-12">--}}
{{--                    <div class="single-process-area wow fadeInLeft" data-wow-delay=".4s">--}}
{{--                        <div class="process-icon">--}}
{{--                            <img src="assets/img/icon/document.png" alt="">--}}
{{--                        </div>--}}
{{--                        <h4>Documentation</h4>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInLeft" data-wow-delay=".6s">--}}
{{--                    <div class="single-process-area">--}}
{{--                        <div class="process-icon">--}}
{{--                            <img src="assets/img/icon/call-center.png" alt="">--}}
{{--                        </div>--}}
{{--                        <h4>Processing</h4>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-3 col-md-6 col-sm-12">--}}
{{--                    <div class="single-process-area wow fadeInLeft" data-wow-delay=".8s">--}}
{{--                        <div class="process-icon">--}}
{{--                            <img src="assets/img/icon/package.png" alt="">--}}
{{--                        </div>--}}
{{--                        <h4>Final Destination</h4>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!-- Quotation Section-->

    <div class="quotation-section bg-cover section-padding">
        <div class="overlay-2"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center">
                    <div class="section-title">
                        <h6>Quotation</h6>
                        <h2>Get Free<b> Quotation</b> for <br>Your <b>Choice</b></h2>
                    </div>
                </div>
            </div>
            <div class="quotation-block wow fadeInUp" data-wow-delay=".3s">
                <form class="quotation-form" method="post" id="request-quote">
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" placeholder="Full Name" required>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="phone">Phone No.</label>
                                <input type="text" class="form-control" id="phone" placeholder="Phone No." required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Select Courier Type</label>
                                <div class="form-group">
                                    <select class="form-control" id="courier-type-box">
                                        <option>Standard</option>
                                        <option>Express</option>
                                        <option>International</option>
                                        <option>Pallet</option>
                                        <option>Warehousing</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Select Location</label>
                                <div class="form-group">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="From" required>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" placeholder="To" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Select Freight</label>
                                <div class="form-group">
                                    <select class="form-control">
                                        <option>Air Freight</option>
                                        <option>Sea Freight</option>
                                        <option>Ground Freight</option>
                                        <option>Rail Freight</option>
                                        <option>Warehousing</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="request-button">
                                <button type="submit" class="main-btn">Send Request<i class="las la-arrow-right"></i></button>
                            </div>
                        </div>
                    </div>

                </form>
                <div class="quotation-dtl text-white">
                    <p><i class="las la-mobile"></i>We are available at Mon-Fri call us +44 7405 239482 during regular business hours</p>
                </div>
            </div>
        </div>
    </div>

    <!-- FAQ Section  -->

{{--    <div class="faq-area section-padding">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6 wow fadeInLeft" data-wow-delay=".3s">--}}
{{--                    <div class="faq-bg bg-cover">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-5 mt-40 wow fadeInRight" data-wow-delay=".4s">--}}
{{--                    <div class="section-title">--}}
{{--                        <h6>Helpful FAQ's</h6>--}}
{{--                        <h2>Frequently Asked <b>Questions</b></h2>--}}
{{--                    </div>--}}
{{--                    <div class="styled-faq">--}}
{{--                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">--}}
{{--                            <div class="panel panel-default">--}}
{{--                                <div class="panel-heading active" role="tab" id="headingOne">--}}
{{--                                    <h6 class="panel-title">--}}
{{--                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">--}}
{{--                                            When can I expect my delivery?--}}
{{--                                            <i class="fa fa-angle-up"></i>--}}
{{--                                            <i class="fa fa-angle-down"></i>--}}
{{--                                        </a>--}}
{{--                                    </h6>--}}
{{--                                </div>--}}
{{--                                <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">--}}
{{--                                    <div class="panel-body">--}}
{{--                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry--}}
{{--                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor--}}
{{--                                        brunch.--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="panel panel-default">--}}
{{--                                <div class="panel-heading" role="tab" id="headingTwo">--}}
{{--                                    <h6 class="panel-title">--}}
{{--                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
{{--                                            What is the first possible pick-up date?--}}
{{--                                            <i class="fa fa-angle-up"></i>--}}
{{--                                            <i class="fa fa-angle-down"></i>--}}
{{--                                        </a>--}}
{{--                                    </h6>--}}
{{--                                </div>--}}
{{--                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">--}}
{{--                                    <div class="panel-body">--}}
{{--                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry--}}
{{--                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor--}}
{{--                                        brunch.--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="panel panel-default">--}}
{{--                                <div class="panel-heading" role="tab" id="headingThree">--}}
{{--                                    <h6 class="panel-title">--}}
{{--                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--}}
{{--                                            How can I pay for the service?--}}
{{--                                            <i class="fa fa-angle-up"></i>--}}
{{--                                            <i class="fa fa-angle-down"></i>--}}
{{--                                        </a>--}}
{{--                                    </h6>--}}
{{--                                </div>--}}
{{--                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">--}}
{{--                                    <div class="panel-body">--}}
{{--                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry--}}
{{--                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor--}}
{{--                                        brunch.--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="panel panel-default">--}}
{{--                                <div class="panel-heading active" role="tab" id="headingSeven">--}}
{{--                                    <h6 class="panel-title">--}}
{{--                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">--}}
{{--                                            How can I collect my parcel in the parcel shop?--}}
{{--                                            <i class="fa fa-angle-up"></i>--}}
{{--                                            <i class="fa fa-angle-down"></i>--}}
{{--                                        </a>--}}
{{--                                    </h6>--}}
{{--                                </div>--}}
{{--                                <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">--}}
{{--                                    <div class="panel-body">--}}
{{--                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry--}}
{{--                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor--}}
{{--                                        brunch.--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!-- Testimonial Section -->

    <div class="testimonial-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="testimonial-carousel owl-carousel">
                        <div class="single-testimonial-item">
                            <div class="testimonial-avatar">
                                <img src="assets/img/testimonial/2.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <div class="testimonial-icon-before"><i class="las la-quote-left"></i></div>
                                <p>
                                    Just a quick note to you to tell you how much we appreciate Ted’s help on two recent shipments to Chicago.
                                    We had a supplier who made a mess of their SID# filing, and this caused a good deal of concern with the local FDA office.
                                    As we got the situation resolved, the case was transferred to a very challenging FDA officer in the local compliance division.
                                    Jemshipping helped us navigate the challenges we faced and got our goods cleared in a week’s time –pretty incredible given the situation.
                                    We just wanted to make you aware of all the help that Jem gave us on this –
                                    we really appreciate your help in supporting our business!”

                                </p>
                                <div class="testimonial-icon-after"><i class="las la-quote-right"></i></div>
                                <h4>Tom Battat <span>CEO</span></h4>
                            </div>
                        </div>

                        <div class="single-testimonial-item">
                            <div class="testimonial-avatar">
                                <img src="assets/img/testimonial/4.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <div class="testimonial-icon-before"><i class="las la-quote-left"></i></div>
                                <p>
                                    Speco Technologies and JemShipping have been working together for over 10 years.
                                    Their staff is knowledgeable and their response time is prompt. American Shipping’s rates are always found to be competitive.
                                    We look forward to continue working with JemShipping
                                    for our freight forwarding and customs brokerage needs in the future.”
                                </p>
                                <div class="testimonial-icon-after"><i class="las la-quote-right"></i></div>
                                <h4>Donna M. Copatillo <span></span></h4>
                            </div>
                        </div>

                        <div class="single-testimonial-item">
                            <div class="testimonial-avatar">
                                <img src="assets/img/testimonial/3.jpg" alt="">
                            </div>
                            <div class="testimonial-content">
                                <div class="testimonial-icon-before"><i class="las la-quote-left"></i></div>
                                <p>
                                    JemShipping has been amazing for the growth of our business from start to finish.
                                    They walked us through all our shipping options with clarity and 100% transparency. Customer service and availability is superb.”
                                </p>
                                <div class="testimonial-icon-after"><i class="las la-quote-right"></i></div>
                                <h4>Andrew M. Lanter <span></span></h4>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Blog Section-->

{{--    <div class="blog-area gray-bg section-padding">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6 col-md-12 col-12">--}}
{{--                    <div class="section-title">--}}
{{--                        <h6>Blog</h6>--}}
{{--                        <h2>What <b>happen</b> inside our <b>company</b></h2>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-6 text-right">--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                    <div class="single-blog-item wow fadeInLeft" data-wow-delay=".4s">--}}
{{--                        <div class="blog-bg">--}}
{{--                            <img src="assets/img/blog/1.jpg" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="blog-content">--}}
{{--                            <p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>25 Feb</p>--}}
{{--                            <h5><a href="single-blog.html">Five Importance logistics solutions for Delivery Services </a>--}}
{{--                            </h5>--}}
{{--                            <p>There are some reason Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, facilis perferendis ipsam.</p>--}}
{{--                            <a href="single-blog.html" class="read-more">Read More</a>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                    <div class="single-blog-item wow fadeInLeft" data-wow-delay=".6s">--}}
{{--                        <div class="blog-bg">--}}
{{--                            <img src="assets/img/blog/2.jpg" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="blog-content">--}}
{{--                            <p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>16 Jan</p>--}}
{{--                            <h5><a href="single-blog.html">Cargo Shipment on Ocean Freight Demands Costly</a></h5>--}}
{{--                            <p>There are some reason Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, facilis perferendis ipsam.</p>--}}
{{--                            <a href="single-blog.html" class="read-more">Read More</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-4 col-md-6 col-sm-12">--}}
{{--                    <div class="single-blog-item wow fadeInLeft" data-wow-delay=".8s">--}}
{{--                        <div class="blog-bg">--}}
{{--                            <img src="assets/img/blog/3.jpg" alt="">--}}
{{--                        </div>--}}
{{--                        <div class="blog-content">--}}
{{--                            <p class="blog-meta"><i class="las la-user-circle"></i>Admin | <i class="las la-calendar-check"></i>01 Jan</p>--}}
{{--                            <h5><a href="single-blog.html">We believe in building long lasting our business relationships</a></h5>--}}
{{--                            <p>There are some reason Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, facilis perferendis ipsam.</p>--}}
{{--                            <a href="single-blog.html" class="read-more">Read More</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!-- Client Area -->

    <div class="client-area pt-50 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="client-carousel owl-carousel">
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/1.png" alt="">
                            </div>
                        </div>
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/2.png" alt="">
                            </div>
                        </div>
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/3.png" alt="">
                            </div>
                        </div>
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/4.png" alt="">
                            </div>
                        </div>
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/5.png" alt="">
                            </div>
                        </div>
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/6.png" alt="">
                            </div>
                        </div>
                        <div class="single-logo-wrapper">
                            <div class="logo-inner-item">
                                <img src="assets/img/client/7.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
    <script>

        $("#request-quote").on("submit", function (e){
            e.preventDefault();
            alert("Your request has been received, Thanks.")
            $(this).trigger("reset");
        })
    </script>
@endsection
